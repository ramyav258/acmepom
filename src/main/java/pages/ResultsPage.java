package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import projectMethod.ProjectMethods;

public class ResultsPage extends ProjectMethods {

	@FindBy(how = How.XPATH, using = "//div[@class='main-container']/div/table//tr[2]/td[1]") WebElement vendorName;
	
	public ResultsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public ResultsPage getVendorName() {
		System.out.println(vendorName.getText());
		return this;
	}
}
