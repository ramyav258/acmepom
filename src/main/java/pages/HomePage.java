package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import projectMethod.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	@FindBy(how = How.XPATH, using ="//i[@class='fa fa-truck']") WebElement vendor;
	@FindBy(how = How.XPATH, using ="//i[@class='fa fa-truck']/parent::button/following::ul[1]/li[1]") WebElement search;
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public SearchPage searchVendor() {
		Actions builder = new Actions(driver);
		builder.moveToElement(vendor).pause(3000)
		.click(search).perform();
		return new SearchPage();
	}
}
