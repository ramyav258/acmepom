package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import projectMethod.ProjectMethods;

public class SearchPage extends ProjectMethods {

	@FindBy(how = How.ID, using = "vendorTaxID") WebElement taxID;
	@FindBy(how = How.ID, using = "buttonSearch") WebElement search;
	
	public SearchPage() {
		PageFactory.initElements(driver, this);
	}
	
	public SearchPage enterTaxID() {
		taxID.sendKeys("IT145632");
		return this;
	}

	public ResultsPage clickSearch() {
		search.click();
		return new ResultsPage();
	}
	
	
	
}
