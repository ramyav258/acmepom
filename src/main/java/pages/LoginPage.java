package pages;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import projectMethod.ProjectMethods;

public class LoginPage extends ProjectMethods{
	
	@FindBy(how = How.ID, using ="email") WebElement uname;
	@FindBy(how = How.ID, using = "password") WebElement pwd;
	@FindBy(how = How.ID, using ="buttonLogin") WebElement login;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@Test
	public LoginPage enterUsername() {
		uname.sendKeys("ramya.v258@gmail.com");
		return this;
	}
	
	@Test
	public LoginPage enterPassword() {
		pwd.sendKeys("ramya@123");
		return this;
	}
	
	@Test
	public HomePage clickLogin() {
		login.click();
		return new HomePage();
	}
}
