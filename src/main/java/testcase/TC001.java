package testcase;

import org.testng.annotations.Test;

import pages.LoginPage;
import projectMethod.ProjectMethods;

public class TC001 extends ProjectMethods {

	@Test
	public void getVendorName() {
		new LoginPage()
		.enterUsername()
		.enterPassword()
		.clickLogin()
		.searchVendor()
		.enterTaxID()
		.clickSearch()
		.getVendorName();
		
	}
}
