package projectMethod;

import org.testng.annotations.BeforeSuite;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class ProjectMethods {
	public static WebDriver driver;
	
	@BeforeSuite
	public void driver() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://acme-test.uipath.com/account/login");
		
	}
	
}
